#include <stdio.h>
#include <stdlib.h>
#include <locale.h>


int menu(int a, int b, int c){
	int op;
	
	
	do{
		nomePrograma();
	
	
		printf("Escolha uma op��o: \n\n");
		printf("1 - Preenche matriz A\n2 - Preenche matriz B\n3 - Somar matrizes A e B\n4 - Exibir matrizes\n5 - Fechar programa\n\n:");
		scanf("%d", &op);
	
		switch(op){
			case 1: preencherMatriz(a, 'A');  return 1; break;
			case 2: preencherMatriz(b, 'B'); return 1;  break;
			case 3: somarMatrizes(a, b, c);  return 1; break;
			case 4: nomePrograma();; exibirMatriz(a, 'A'); exibirMatriz(b, 'B'); exibirMatriz(c, 'C'); system("pause"); return 1; break;
			case 5: return 0; break;
			default: nomePrograma(); printf("OP��O INV�LIDA\n\n"); system("pause"); return 1; break;
			
		}
	}while(op < 1 || op >5);
	
}

void exibirMatriz(int x[3][3], char nome){

	int i, j;
	printf("\n===== Matriz %c =====\n\n", nome);
	for(i = 0; i < 3; i++){
		for(j = 0; j < 3; j++){
			printf(" %d ",x[i][j]);
		}
		printf("\n");
	}
}

void nomePrograma(){
	system("cls");
	printf("\n========== Somador de Matrizes ==========\n\n");
}

void preencherMatriz(int x[3][3], char nome[1]){
	nomePrograma();
	printf("Preencha a matriz %c:\n\n", nome);
	int i, j;
	for(i = 0; i < 3; i++){
		for(j = 0; j < 3; j++){
			printf("(%d, %d): ",i+1, j+1);
			scanf("%d", &x[i][j]);
		}
	}
}

void somarMatrizes(int a[3][3], int b[3][3], int c[3][3]){
	nomePrograma();
	printf("Matrizes somadas\n\n");
	system("pause");
	int i, j;
	for(i = 0; i < 3; i++){
		for(j = 0; j < 3; j++){
			c[i][j] = a[i][j] + b[i][j];
		}
	}
}


int main(int argc, char *argv[]){
	int a[3][3], b[3][3], c[3][3], fim = 1;
	setlocale(LC_ALL,"Portuguese");
	
	do{
		fim =menu(a, b, c);
	}while(fim != 0);
	printf("\nPrograma finalizado!");
	
	return 0;
}
