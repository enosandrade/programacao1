#include <stdio.h>
#include <stdlib.h>
#include <locale.h>


void calcular(float n1, float n2, int operacao){
	switch(operacao){
		case 1: printf("\n%.1f + %.1f = %.1f", n1, n2, n1 + n2); break;
		case 2: printf("\n%.1f - %.1f = %.1f", n1, n2, n1 - n2); break;
		case 3: printf("\n%.1f * %.1f = %.1f", n1, n2, n1 * n2); break;
		case 4:
			if(n2 == 0){
				do{
					printf("\nA divis�o por 0 � impossivel.\n");
					printf("Digite um denominador diferente de 0: ");
					scanf("%f", &n2);
					
				}while(n2 == 0);
				
				printf("\n%.1f / %.1f = %.1f", n1, n2, n1 / n2); break;
				break;	
			}else{
				printf("\n%.1f / %.1f = %.1f", n1, n2, n1 / n2); break;
			}
	}
}



int main(int argc, char *argv[]) {
	setlocale(LC_ALL,"Portuguese");
	int continuar, op;
	float n1, n2;

	do{
		system("cls");
		printf("\n-------------------- Calculadora --------------------\n\n");
		
		printf("Digite o primeiro n�mero: ");
		scanf("%f", &n1);
		
		printf("Digite o segundo n�mero: ");
		scanf("%f", &n2);
		
		do{
			printf("\nEscolha uma opera��o:\n\n1 - Somar\n2 - Subtrair\n3 - Multiplicar\n4 - Dividir\n\n:");
			scanf("%d", &op);
			if(op < 1 || op > 4){
				printf("\nOP��O INV�LIDA");
			}
		}while(op < 1 || op > 4);
		
		calcular(n1, n2, op);
		
		
		do{
			printf("\n\nDesejar resolver outro calculo? (1 - SIM ou 0 - N�O): ");
			scanf("%d", &continuar);
			
			if(continuar == 0){
				printf("\n\nPrograma finalizado.");
			}else{
				if(continuar != 1 && continuar != 0){
					printf("\nOP��O INV�LIDA");
				}
			}
			
		}while(continuar != 1 && continuar != 0);
		
	}while(continuar == 1);
	
	return 0;
}
