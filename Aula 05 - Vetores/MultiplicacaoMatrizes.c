#include <stdio.h>
#include <stdlib.h>
#include <locale.h>


int main(int argc, char *argv[]) {
	setlocale(LC_ALL, "Portuguese");
	int linA, linB, colA, colB, linha, coluna, temp;
	
	
	printf("=============== Multiplica��o de Matrizes ===============\n\n");
	
	printf("Linhas Matriz A: ");
	scanf("%d", &linA);
	printf("Colunas Matriz A: ");
	scanf("%d", &colA);
	printf("Linhas Matriz B: ");
	scanf("%d", &linB);
	printf("Colunas Matriz B: ");
	scanf("%d", &colB);
	
	
	
	if(colA == linB){
		int mA[linA][colA], mB[linB][colB], mC[linA][colB];
		
		
		printf("\nPreencha a matriz A: \n\n");
		for(linha = 0; linha < linA; linha++){  //Preenche a matriz A
			for(coluna = 0; coluna <colA; coluna++){
				printf("(%d, %d): ",linha+1, coluna+1);
				scanf("%d",&mA[linha][coluna]);
			}
		}
		
		printf("\nPreencha a matriz B: \n\n");
		for(linha = 0; linha < linB; linha++){  //Preenche a matriz B
			for(coluna = 0; coluna <colB; coluna++){
				printf("(%d, %d): ",linha+1, coluna+1);
				scanf("%d",&mB[linha][coluna]);
			}
		}
		
		//Realiza a multiplica��o
		
		int i;
		for(linha = 0; linha < linA; linha++){
			for(coluna = 0; coluna < colB; coluna++){
				temp = 0;
				for(i = 0; i < colA; i++){
					temp+= mA[linha][i] *mB[i][coluna];
				}
				mC[linha][coluna] = temp;
			}
		}
		
		//Imprime as matrizes
		printf("\nMatriz A:\n\n");
		for(linha = 0; linha < linA; linha++){
			for(coluna = 0; coluna <colA; coluna++){
				printf(" %d ", mA[linha][coluna]);
			}
			printf("\n");
		}
		
		printf("\nMatriz B:\n\n");
		for(linha = 0; linha < linB; linha++){ // Exibe a Matriz B

			for(coluna = 0; coluna <colB; coluna++){
				printf("  %d  ",mB[linha][coluna]);
			}
			printf("\n");
			
		}
		
		printf("\nMatriz B (A x B):\n\n");
		for(linha = 0; linha < linA; linha++){ // Exibe a Matriz B

			for(coluna = 0; coluna <colB; coluna++){
				printf("  %d  ",mC[linha][coluna]);
			}
			printf("\n");
			
		}
		
	}else{
		printf("\nImposs�vel multiplicar as matrizes A e B.\nA quantidade de colunas de A � diferente da quantidade  de linhas de B.\n\n");
	}
		
	system("pause");
	return 0;
}
