#include <stdio.h>
#include <stdlib.h>
#include <locale.h>



int main(int argc, char *argv[]) {
	setlocale(LC_ALL,"Portuguese");
	
	int a[3][4], b[3][4], c[3][4], linha, coluna,continuar;
	
	do{
		
		system("cls");
		printf("=============== Subtra��o de Matrizes ===============\n\n");
		
		printf("Preencha a matriz A: \n\n");
		for(linha = 0; linha < 3; linha++){  //Preenche a matriz A
			for(coluna = 0; coluna <4; coluna++){
				printf("(%d, %d): ",linha, coluna);
				scanf("%d",&a[linha][coluna]);
			}
		}
		system("cls");
		
		printf("\nPreencha a matriz B: \n\n");
		for(linha = 0; linha < 3; linha++){ //Preenche a Matriz B
			for(coluna = 0; coluna <4; coluna++){
				printf("(%d, %d): ",linha, coluna);
				scanf("%d",&b[linha][coluna]);
			}
		}
		
		for(linha = 0; linha < 3; linha++){ // Calcula A - B
			for(coluna = 0; coluna <4; coluna++){
				c[linha][coluna] = a[linha][coluna] - b[linha][coluna];
			}
		}
		system("cls");
		printf("\nMatriz A:\n\n");
		for(linha = 0; linha < 3; linha++){ // Exibe a Matriz A

			for(coluna = 0; coluna <4; coluna++){
				printf("  %d  ",a[linha][coluna]);
			}
			printf("\n");
			
		}
		
		printf("\nMatriz B:\n\n");
		for(linha = 0; linha < 3; linha++){ // Exibe a Matriz B
			for(coluna = 0; coluna <4; coluna++){
				printf("  %d  ",b[linha][coluna]);
			}
			printf("\n");
		}
		
		printf("\nMatriz C (A - B):\n\n");
		for(linha = 0; linha < 3; linha++){ // Exibe a Matriz C
			for(coluna = 0; coluna <4; coluna++){
				printf("  %d  ",c[linha][coluna]);
			}
			printf("\n");
		}
		
		do{
			printf("\n\nDesejar resolver outra Subtra��o de Matrizes? (1 - SIM ou 0 - N�O): ");
			scanf("%d", &continuar);
			
			if(continuar == 0){
				printf("\n\nPrograma finalizado.");
			}else if(continuar != 1 || continuar  != 0){
				printf("\nOP��O INV�LIDA");
			}
		}while(continuar != 1 && continuar != 0);
	}while(continuar == 1);
	return 0;
}
