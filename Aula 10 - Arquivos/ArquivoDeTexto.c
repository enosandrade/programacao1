#include <stdio.h>
#include <stdlib.h>
#include <locale.h>


int main(int argc, char *argv[]) {
	setlocale(LC_ALL,"Portuguese");
	
	char string[100];
	FILE *p = fopen("arquivo.txt","a");
	if(p == NULL){
		printf("Erro na abertura do arquivo.\n");
		system("pause");
		exit(1);
		
	}
	do{
		fflush(stdin);
		printf("Escreva no arquivo: ");
		gets(string);
		if(strcmp("0",string) == 0){
			printf("\nVOC� DIGITOU ZERO E ENCERROU A ESCRITA\n\n");			
			
		}else{
			int i;
			for(i = 0; i < strlen(string); i++){
				fputc(string[i], p);
			}
			printf("\nTEXTO ADICIONADO AO FIM DO ARQUIVO\n\n");
			
		}
		system("pause");
		system("cls");
	}while(strcmp("0",string) != 0);
	fclose(p);
	
	p = fopen("arquivo.txt","r");
	
	char c, leitura[100];
	int i = 0;
	while((c = fgetc(p)) != EOF){
		leitura[i] = c;
		i++;
	}
	printf("O arquivo cont�m o seguinte texto: %s\n\n",leitura);
	printf("A quantidade de caracteres armazenada � %d", strlen(leitura));
	
	return 0;
}
