#include <stdio.h>
#include <stdlib.h>
#include <locale.h>

struct dataNascimento{
	int dia, mes, ano;
};
int qtdContatos;
FILE *f, *aux;
char caminho[]="agenda.bin";

struct contato{
	char nome[50], telefone[20];

	struct dataNascimento dn;
};

void cabecalho(){
	system("cls");
	printf("========== AGENDA DE CONTATOS ==========\n\n");
}

verificarAbertura(FILE *f){
	if(f == NULL){
		cabecalho();
		printf("ERRO AO LER DADOS\n\n");
		system("pause");
	}
}


void opcaoInvalida(){
	cabecalho();
	printf("OPÇÃO INVÁLIDA!\n\n");
	system("pause");
}
void inserirContato(){
	cabecalho();
	struct contato contato;
	f = fopen(caminho,"ab");
	verificarAbertura(f);
	printf("===== INSERIR CONTATO =====\n\n");
	fflush(stdin);
	printf("Nome: ");
	gets(contato.nome);
	printf("Telefone: ");
	gets(contato.telefone);
	fflush(stdin);
	printf("\nData de nascimento:\nDia: ");
	scanf("%d",&contato.dn.dia);
	printf("M�s: ");
	scanf("%d",&contato.dn.mes);
	printf("Ano: ");
	scanf("%d",&contato.dn.ano);
	fwrite(&contato,sizeof(struct contato),1,f);
	fclose(f);
	printf("\nCONTATO ADICIONADO COM SUCESSO\n\n");
	system("pause");
}
void removerContato(){
	cabecalho();
	printf("===== REMOVER CONTATO =====\n\n");

	f = fopen(caminho,"rb");
	verificarAbertura(f);

	aux = fopen("auxiliar.bin","wb");
	verificarAbertura(aux);

	struct contato contato;

	char nome[50],apagar;
	fflush(stdin);
	printf("Nome do contato que deseja remover: ");
	gets(nome);
 	int qtd;

	while(fread(&contato,sizeof(struct contato),1,f)==1){
		if(strcmp(contato.nome, nome)==0){
			qtd++;
			printf("\n");

			do{
				cabecalho();
				printf("===== REMOVER CONTATO =====\n\n");
				apresentarContato(contato);
				fflush(stdin);
				printf("Deseja realmente apagar contato? (S/N) ");
				scanf("%c", &apagar);

				if(apagar != 'n' && apagar != 'N' && apagar != 's' && apagar != 'S'){
					printf("\nOP��O INV�LIDA!\n\n");
					system("pause");
				}
			}while(apagar != 'n'&& apagar != 'N'&& apagar != 's' && apagar != 'S');


			if(apagar == 'n'|| apagar =='N'){
				printf("\nCONTATO MANTIDO NA AGENDA\n\n");
				fwrite(&contato,sizeof(struct contato),1,aux);
			}else{
				printf("\nCONTATO APAGADO DA AGENDA\n\n");
			}
		}else{
			fwrite(&contato,sizeof(struct contato),1,aux);
		}
	}
	if(qtd==0){
		printf("\nCONTATO COM ESSE NOME N�O ENCONTRADO\n\n");
	}

	fclose(aux);
	fclose(f);
	remove("agenda.bin");
	rename("auxiliar.bin","agenda.bin");

	system("pause");

}

void pesquisarContato(){
	cabecalho();
	printf("===== PESQUISAR CONTATO  PELO NOME =====\n\n");
	char nome[50];
	fflush(stdin);
	printf("Digite o nome do contato: ");
	gets(nome);
	struct contato contato;
	int encontrado = 0;

	f = fopen(caminho,"rb");
	verificarAbertura(f);

	while(fread(&contato,sizeof(struct contato),1,f)==1){
		if(strcmp(contato.nome, nome)==0){
			printf("\n");
			apresentarContato(contato);
			encontrado++;
		}
	}
	if(encontrado == 0){
		printf("\n\nCONTATO N�O ENCONTRADO\n\n");
	}
	fclose(f);
	system("pause");

}

apresentarContato(struct contato contato){
	printf("===== Contato =====\n\n");
	printf("Nome: %s\n",contato.nome);
	printf("Telefone: %s\n",contato.telefone);
	printf("Data de nascimento: %d\\%d\\%d\n\n",contato.dn.dia,contato.dn.mes, contato.dn.ano);
}

void listarContatos(){
	cabecalho();
	printf("===== LISTA DE CONTATOS =====\n\n");
	f = fopen(caminho,"rb");
	verificarAbertura(f);
	int qtd =0;
	struct contato contato;

	while(fread(&contato,sizeof(struct contato),1,f)==1){
		qtd++;
		apresentarContato(contato);

	}
	if(qtd == 0){
		printf("VOC� AINDA N�O ADCIONOU CONTATOS A AGENDA\n\n");
	}
	fclose(f);
	system("pause");

}
 void listarA(){
 	cabecalho();
 	printf("===== LISTA DE CONTATOS INICIADOS PELA LETRA 'A' =====\n\n");

	f = fopen(caminho,"rb");
	verificarAbertura(f);
	int qtd = 0;
	struct contato contato;

	while(fread(&contato,sizeof(struct contato),1,f)==1){
		if(contato.nome[0] == 'a'|| contato.nome[0] == 'A'){
			qtd++;
			apresentarContato(contato);
		}
	}

	if(qtd == 0){
		printf("N�O H� CONTATOS COM A LETRA 'A'\n\n");
	}

	fclose(f);
	system("pause");

 }
 void listarAniversariante(){
 	cabecalho();
 	printf("===== LISTA DE ANIVERSARIANTES DO M�S =====\n\n");

	f = fopen(caminho,"rb");
	verificarAbertura(f);

	int qtd = 0;
	struct contato contato;
	int mes;
	printf("Escolha o m�s (n�mero): ");
	scanf("%d", &mes);
	printf("\n");
	while(fread(&contato,sizeof(struct contato),1,f)==1){
		if(contato.dn.mes == mes){
			qtd++;
			apresentarContato(contato);
		}
	}

	if(qtd == 0){
		printf("N�O H� ANIVERSARIANTES ESTE M�S\n\n");
	}

	fclose(f);
	system("pause");


 }
int menu(){
	int op;
	cabecalho();
	printf("===== MENU =====\n\n");
	printf("1 - Inserir contato\n2 - Remover contato\n3 - Pesquisar contato pelo nome\n4 - Listar todos os contatos\n5 - Listar contatos cujo nome come�a pela letra 'a'\n6 - Listar os aniversariantes do m�s\n7 - Fechar programar\n\n:");
	scanf("%d", &op);
	switch(op){
		case 1:	inserirContato(); return 0; break;
		case 2: removerContato(); return 0; break;
		case 3: pesquisarContato(); return 0; break;
		case 4: listarContatos(); return 0; break;
		case 5: listarA(); return 0;break;
		case 6: listarAniversariante(); return 0; break;
		case 7: return 1; break;
		default: opcaoInvalida();
	}
}

int main(int argc, char *argv[]) {
	setlocale(LC_ALL,"Portuguese");
	int parar;
	do{
		parar = menu();
	}while(parar != 1);
	printf("\nAgenda encerrada\n");
	return 0;
}
