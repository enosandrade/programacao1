#include <stdio.h>
#include <stdlib.h>
#include <locale.h>


int main(int argc, char *argv[]) {
	setlocale(LC_ALL,"Portuguese");
	FILE *f;
	char caminho[100];

	
	do
	{
		system("cls");
		printf("Caminho do arquivo: ");
		gets(caminho);
		f = fopen(caminho, "r");
		if(f == NULL){
			printf("Caminho inv�lido!\n");
			system("pause");
		}
	}while(f == NULL);
	
	char c, conteudo[1000], l;
	int i = 0, vogais = 0, consoantes= 0;
	 
	while((c = fgetc(f)) != EOF){
		conteudo[i] =c;
		l = toupper(c);
		if(l == 'A'|| l == 'E'|| l == 'I'|| l== 'O'|| l=='U'){
			vogais++;
		}else if(l == 'B'|| l == 'C'|| l == 'D'|| l == 'F'|| l == 'G'|| l == 'H'|| l == 'I'|| l == 'J'|| l == 'K'
		|| l == 'L'|| l == 'M'|| l == 'N'|| l == 'P'|| l == 'Q'|| l == 'R'|| l == 'S'|| l == 'T'|| l == 'V'|| l == 'W'
		|| l == 'X'|| l == 'Y'|| l == 'Z'){
			consoantes++;
		}
	
		i++;
	}
	printf("Esse � conte�do do arquivo: %s\n",conteudo);
	printf("Vogais no arquivo: %d\n", vogais);
	printf("Consoantes no arquivo: %d\n", consoantes);
	return 0;
}
