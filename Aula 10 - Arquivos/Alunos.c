#include <stdio.h>
#include <stdlib.h>
#include <locale.h>

FILE *f;
char caminho[] = "alunos.bin";

struct aluno{
	char nome[50], disciplina[50];

	
	float n1, n2, n3, n4;
};

struct aluno aluno;
struct aluno aux;

int main(int argc, char *argv[]) {
	setlocale(LC_ALL,"Portuguese");
	int parar;
	do{
		parar = menu();
	}while(parar != 1);
	printf("\nPROGRAMA ENCERRADO\n");
	return 0;
}

//MENU
int menu(){
	int op;
	cabecalho();
	printf("===== MENU =====\n\n");
	printf("1 - INSERIR ALUNO E NOTAS\n2 - SALVAR EM DISCO\n3 - EXIBIR ALUNOS E M?IAS\n4 - EXIBIR ALUNOS APROVADOS\n5 - EXIBIR ALUNOS REPROVADOS\n6 - SAIR DO PROGRAMA\n\n:");
	scanf("%d", &op);
	switch(op){
		case 1:	inserirAluno(); return 0; break;
		case 2: salvarAluno(); return 0; break;
		case 3: exibirAlunos(); return 0; break;
		case 4: exibirAlunosAprovados(); return 0; break;
		case 5: exibirAlunosReprovados(); return 0;break;
		case 6: return 1; break;
		default: opcaoInvalida(); break;
	}
}

//OPÇÕES DO MENU

void inserirAluno(){
  cabecalho();
  printf("===== INSERIR ALUNO E NOTAS =====\n\n");

  printf("NOME: ");
  fflush(stdin);
  gets(aluno.nome);

  printf("DISCIPLINA: ");
  fflush(stdin);
  gets(aluno.disciplina);

  printf("NOTA 1: ");
  scanf("%f", &aluno.n1);

  printf("NOTA 2: ");
  scanf("%f", &aluno.n2);

  printf("NOTA 3: ");
  scanf("%f", &aluno.n3);

  printf("NOTA 4: ");
  scanf("%f", &aluno.n4);
}
void salvarAluno(){
	
	f = fopen(caminho, "ab");
	verificarAbertura(f);
	
	cabecalho();
  	printf("===== SALVAR EM DISCO =====\n\n");
  	if(strlen(aluno.nome) == 0){
  		printf("?NECESS?RIO INSERIR ALUNO ANTES DE SALVAR NO DISCO\n\n");
  		system("pause");
	}else{
  		fwrite(&aluno,sizeof(struct aluno),1,f);
		printf("ALUNO SALVO COM SUCESSO\n\n");
		aluno = aux;
		system("pause");
	}
	
	fclose(f);
}



void exibirAlunos(){
	f = fopen(caminho, "rb");
	verificarAbertura(f);
	
	int qtd = 0;
	cabecalho();
  	printf("===== EXIBIR ALUNOS E M?IAS =====");
  	while(fread(&aluno,sizeof(struct aluno),1, f) == 1){
  		qtd++;
  		mostrarAluno(aluno);
	}
	if(qtd == 0){
		printf("\n\nNENHUM ALUNO CADASTRADO");
	}
	printf("\n\n");
	system("pause");
	fclose(f);
}

void exibirAlunosAprovados(){
	f = fopen(caminho, "rb");
	verificarAbertura(f);
	
	int qtd = 0;
	cabecalho();
  	printf("===== EXIBIR ALUNOS APROVADOS =====");
  	while(fread(&aluno,sizeof(struct aluno),1, f) == 1){
  		if(verificarAprovacao(aluno) == 1){
  			qtd++;
  			mostrarAluno(aluno);	
		}
	}
	if(qtd == 0){
		printf("\n\nNENHUM ALUNO APROVADO");
	}
	printf("\n\n");
	system("pause");
	fclose(f);
}

void exibirAlunosReprovados(){
	f = fopen(caminho, "rb");
	verificarAbertura(f);
	
	int qtd = 0;
	cabecalho();
  	printf("===== EXIBIR ALUNOS REPROVADOS =====");
  	while(fread(&aluno,sizeof(struct aluno),1, f) == 1){
  		if(verificarAprovacao(aluno) == 0){
  			qtd++;
  			mostrarAluno(aluno);	
		}
	}
	if(qtd == 0){
		printf("\n\nNENHUM ALUNO REPROVADO");
	}
	printf("\n\n");
	system("pause");
	fclose(f);
}

//UTILITARIOS

int verificarAprovacao(struct aluno aluno){
	float media = (aluno.n1 + aluno.n2 + aluno.n3 + aluno.n4 )/4;
	return media >= 7 ? 1 : 0;
	
}

void mostrarAluno(struct aluno aluno){
	printf("\n\n===== ALUNO =====\n\n");
	printf("NOME: %s\n", aluno.nome);
	printf("DISCIPLINA: %s\n", aluno.disciplina);
	printf("NOTA 1: %.1f\n", aluno.n1);
	printf("NOTA 2: %.1f\n", aluno.n2);
	printf("NOTA 3: %.1f\n", aluno.n3);
	printf("NOTA 4: %.1f\n", aluno.n4);
	float media = (aluno.n1 + aluno.n2 + aluno.n3 + aluno.n4 )/4;
	printf("M?IA: %.1f", media);
}
void cabecalho(){
	system("cls");
	printf("========== GERENCIAR ALUNOS ==========\n\n");
}

verificarAbertura(FILE *f){
	if(f == NULL){
		cabecalho();
		printf("ERRO AO LER DADOS\n\n");
		system("pause");
	}
}

void opcaoInvalida(){
	cabecalho();
	printf("OP?O INV?LIDA!\n\n");
	system("pause");
}
