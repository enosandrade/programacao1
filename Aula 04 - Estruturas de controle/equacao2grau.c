#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#include <math.h>

int main(int argc, char *argv[]) {
	setlocale(LC_ALL,"Portuguese");
	int continuar;
	float a, b, c, delta, x1, x2;
	do{
		system("cls");
		printf("\n\n=============== Equa��o do Segundo Grau ===============\n\n");
		
		
		printf("Valor de a: ");
		scanf("%f", &a);
		printf("Valor de b: ");
		scanf("%f",&b);
		printf("Valor de c: ");
		scanf("%f", &c);
		
		delta = (b*b)-4*a*c;
		
		if(a == 0){
			printf("\nEm uma Equa��o de Segundo Grau, o 'a' n�o pode ser igual a 0.");
		}else if(delta > 0){
			printf("\nDelta = %.0f\n", delta);
			
			printf("\nA equa��o possui duas ra�zes reais: \n");
		
			x1 =(-b +sqrt(delta))/2*a;
			x2 =(-b -sqrt(delta))/2*a;
			printf("\nx' = %.0f",x1);
			printf("\nx'' = %.0f",x2);
			
		}else if(delta == 0){
			printf("\nDelta = %.0f\n", delta);
			
			printf("\nA equa��o possui apenas uma ra�z real: \n");
			x1 =(-b +sqrt(delta))/2*a;
			printf("\nx = %.0f",x1);
		}else{
			printf("\nDelta = %.0f\n", delta);
			
			printf("\n\nA equa��o n�o possui ra�zes reais.");
		}
		
		do{
			printf("\n\nDesejar resolver outra equa��o? (1 - SIM ou 0 - N�O): ");
			scanf("%d", &continuar);
			
			if(continuar == 0){
				printf("\n\nPrograma finalizado.");
			}else if(continuar != 1 || continuar  != 0){
				printf("\nOP��O INV�LIDA");
			}
		}while(continuar != 1 && continuar != 0);
		
	}while(continuar == 1);
	
	return 0;
}
