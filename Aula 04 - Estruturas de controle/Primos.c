#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
/* run this program using the console pauser or add your own getch, system("pause") or input loop */

int main(int argc, char *argv[]) {
	setlocale(LC_ALL, "Portuguese");
	int numero, divisor, atual, total;
	total = 1;
	printf("==================== N�meros Primos ====================\n\n");
	printf("Digite um n�mero: ");
	scanf("%d",&numero);
	printf("\nExistem os seguintes n�meros primos entre 0 e %d:",numero);
	printf("\n\n2");
	
	atual = 3;
	while(atual <= numero){
	
		for(divisor = atual-1; divisor>1; divisor--){
			if(atual%divisor == 0){
				break;
			}else if(divisor == 2){
				total++;
				printf(", %d", atual);			
			}
		}
	atual++;
	}
	printf("\n\nTotal de n�meros primos encontrados: %d",total);
	return 0;
}
