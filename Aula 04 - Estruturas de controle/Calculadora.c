#include <stdio.h>
#include <stdlib.h>
#include <locale.h>


int main(int argc, char *argv[]) {
	setlocale(LC_ALL, "Portuguese");
	float n1, n2;
	int opcao, continuar;
	printf("==================== Calculadora ====================\n");
	
	//Menu
	do{
		do{
			printf("\nSelecione uma das op��es: \n\n1 - Somar\n2 - Subtrair\n3 - Multiplicar\n4 - Dividir\n\n:"); // menu
			scanf("%d",&opcao);
			
			if(opcao <1 || opcao > 4){
				printf("\nOP��O INV�LIDA!\n"); //Trata op��o inv�lida no menu
			}
		}while(opcao <1 || opcao > 4);
		
		//Recebe n�meros
		printf("\nN�mero 1: ");
		scanf("%f", &n1);
		printf("N�mero 2: ");
		scanf("%f", &n2);
		
		//realiza os calculos
		switch(opcao){
			case 1:
				printf("\n%.2f + %.2f = %.2f.",n1, n2, n1 +n2 );
				break;
			case 2:
				printf("\n%.2f - %.2f = %.2f.", n1, n2, n1 - n2);
				break;
			case 3:
				printf("\n%.2f * %.2f = %.2f.", n1, n2, n1 * n2);
				break;
			case 4:
				if(n2 == 0){
					printf("\n� imposs�vel dividir um n�mero real por 0.");
				}else{
					printf("\n%.2f / %.2f = %.2f.\n", n1, n2, n1 / n2);
				}
				break;
		}
		//verifica se o usuario deseja continuar calculando
		do{
			printf("\nDesejar realizar outra opera��o? (1 - SIM ou 0 - N�O): ");
			scanf("%d", &continuar);
			if(continuar != 1 && continuar != 0){
				printf("\nOP��O INV�LIDA");
			}
		}while(continuar != 1 && continuar != 0);
		printf("\nCalculadora finalizada.");
	}while(continuar == 1);
	return 0;
}
