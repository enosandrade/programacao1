#include <stdio.h>
#include <stdlib.h>
#include <locale.h>


int main(int argc, char *argv[]) {
		int ordem, linha, coluna;
	
		setlocale(LC_ALL,"Portuguese");
		
		printf("=============== Matriz Identidade ===============\n\n");
		printf("Ordem da matriz: ");
		scanf("%d", &ordem);
		printf("\n\n");
		for(linha = 1; linha <= ordem; linha++){
			for(coluna = 1; coluna <= ordem; coluna++){
				if(linha == coluna)	{
					printf("  1  ");
				}else{
					printf("  0  ");
				}
			}
			printf("\n");
			
		}
		
	return 0;
}
