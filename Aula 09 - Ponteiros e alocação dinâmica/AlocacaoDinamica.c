#include <stdio.h>
#include <stdlib.h>
#include <locale.h>


int main()
{
	setlocale(LC_ALL,"Portuguese");
	
	int tamanho;
	printf("Quantos elementos ter� seu vetor: ");
	scanf("%d", &tamanho);

	int *p = (int *) calloc(tamanho, sizeof(int));
	if (p == NULL) {
		printf("M�moria insuficiente!");
	}
	int i;
	
	for (i = 0; i < tamanho; i++) {
		printf("Digite o valor %d: ", i+1);
		scanf("%d", &*(p + i));
	}
	
	printf("\n\nVoc� digitou os seguintes valores:\n\n");
	for(i = 0; i <tamanho; i++){
		printf(" %d ",*(p+i));
	}
	return 0;

}

