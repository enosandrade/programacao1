#include <stdio.h>
#include <stdlib.h>
#include <locale.h>

void mostrar(int tamanho, int *p){
	int i;
	printf("\n\nVoc� digitou os seguintes valores:\n\n");
	for(i = 0; i <tamanho; i++){
		printf(" %d ",p[i]);
	}
	
}

int main()
{
	setlocale(LC_ALL,"Portuguese");
	
	int tamanho;
	printf("Quantos elementos ter� seu vetor: ");
	scanf("%d", &tamanho);

	int *p = (int *) calloc(tamanho, sizeof(int));
	if (p == NULL) {
		printf("M�moria insuficiente!");
	}
	int i;
	printf("\n");
	for (i = 0; i < tamanho; i++) {
		printf("Digite o valor %d: ", i+1);
		scanf("%d", &p[i]);
	}
	
	mostrar(tamanho, p);
	
	
	p = realloc(p, (tamanho-5)*sizeof(int));
	
	mostrar(tamanho-5, p);
	
	p = realloc(p, tamanho*sizeof(int));
	
	mostrar(tamanho, p);
	return 0;

}

