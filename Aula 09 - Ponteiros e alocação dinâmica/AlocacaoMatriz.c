#include <stdio.h>
#include <stdlib.h>
#include <locale.h>

int main(){
	setlocale(LC_ALL, "Portuguese");
	
	int l;
	
	printf("Quantas linhas ter� a matriz? ");
	scanf("%d", &l);
	
	printf("\n");
	int *p;
	
	p = (int *)calloc(4*l,sizeof(int));
	
	if(p == NULL){
		printf("Mem�ria insuficiente!");
		exit(1);
	}
	
	
	//Preenchendo a matriz
	int i;
	for(i = 0; i<4*l; i++){
		*(p+i) = i+1;
	}
	
	//Mostrando a matriz
	for(i = 0; i<4*l; i++){
	
		printf(" %d ",*(p+i));
		if((i+1)%4==0){
			printf("\n");
		}
	}
	return 0;
}
