#include <stdio.h>
#include <stdlib.h>
#include <locale.h>

int main(){
	setlocale(LC_ALL, "Portuguese");
	
	int x = 20; // atribui 20 a x
 	float *p =  &x; // o ponteiro *p aponta para o endere�o de x
	
	printf("O valor de x � %d\n", x);//saida: 20
	printf("O valor de p � %d\n", p);//saida: &x
	printf ("O valor de *p � %d\n", *(int*)p);//saida: 20
	    
	return 0;
}
