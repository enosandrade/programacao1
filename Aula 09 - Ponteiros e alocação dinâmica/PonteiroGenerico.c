#include <stdio.h>
#include <stdlib.h>
#include <locale.h>

int main(){
	setlocale(LC_ALL, "Portuguese");
	
	char c = 'e'; //atribui 'e' a c
	
	void *p = &c; //ponteiro *p aponponteiroa vari�vel c
	
	void **pp = &p; // ponteiro **pp aponta prq o ponteiro *p
	
	void  ***ppp = &pp; //ponteiro ***ppp aponta para o ponteiro **pp
	
	printf("c vale %c\n",c); // imprime o conteuda variavel c
	printf("p vale %d\n",p);// imprime o endere�o apontado por *p
	printf("pp vale %d\n",pp); // imprime o endere�o apontado po **pp
	printf("ppp vale %d\n",ppp);// imprime o endere�o apontado po ***ppp
	printf("*p vale %c\n",*(char*)p); //imprime o conteudo do endere�o apontado por *p
	printf("**pp vale %c\n",**(char**)pp);//imprime o conteudo do conteudo do endere�o apontado por **pp
	printf("***ppp vale %c\n",***(char ***)ppp);//imprime o conteudo do conteudo do conteudo do endere�o apontado por ***ppp
	    
	return 0;
}
