#include "Cliente.h"
#include <locale.h>

void imprimeCliente(char* nome, char sexo,int idade, int numCasa, char* rua, char* bairro, char* cidade,
	char* telefone, char* email, float peso, float altura, float mensalidade, char* dataPagamento){
		
	printf("=============== Cliente ===============\n\n");
	printf("Nome: %s\n", nome);
	printf("Sexo: %s\n", sexo == 'F'? "Feminino": "Masculino");
	printf("Idade: %d\n", idade);
	printf("Altura: %.2f\n", altura);
	printf("Peso: %.2f\n", peso);
	printf("Rua: %s\n", rua);
	printf("Bairro: %s\n", bairro);
	printf("Cidade: %s\n", cidade);
	printf("N�mero da casa: %d\n", numCasa);
	printf("Telefone: %s\n", telefone);
	printf("Email: %s\n", email);
	printf("Valor da mensalidade: %.2f\n", mensalidade);
	printf("Data de pagamento: %s\n",dataPagamento);
	printf("\n");
}

int main(){
	setlocale(LC_ALL,"Portuguese");
	Cliente *c1, *c2, *c3;
	
	char nome[30];
	char sexo;
	int idade;
	int numCasa;
	char rua[20];
	char bairro[30];
	char cidade[30];
	char telefone[15];
	char email[20];
	float peso;
	float altura;
	float mensalidade;
	char dataPagamento[20];
	
	

	c1 = criarCliente();
	c2 = criarCliente();
	c3 = criarCliente();

	lerDadosCliente(c1, &nome, &sexo, &idade, &numCasa, &rua, &bairro, &cidade, &telefone, &email, &peso, &altura, &mensalidade, &dataPagamento);
	imprimeCliente(nome, sexo, idade, numCasa, rua, bairro, cidade,  telefone, email, peso, altura, mensalidade, dataPagamento);

	lerDadosCliente(c2, &nome, &sexo,&idade, &numCasa, &rua, &bairro, &cidade, &telefone, &email, &peso, &altura, &mensalidade, &dataPagamento);
	imprimeCliente(nome, sexo, idade, numCasa, rua, bairro, cidade,  telefone, email, peso, altura, mensalidade, dataPagamento);

	lerDadosCliente(c3, &nome, &sexo,&idade, &numCasa, &rua, &bairro, &cidade, &telefone, &email, &peso, &altura, &mensalidade, &dataPagamento);
	imprimeCliente(nome, sexo, idade, numCasa, rua, bairro, cidade,  telefone, email, peso, altura, mensalidade, dataPagamento);

	escreveDadosCliente(c1);
	lerDadosCliente(c1, &nome, &sexo,&idade, &numCasa, &rua, &bairro, &cidade, &telefone, &email, &peso, &altura, &mensalidade, &dataPagamento);
	imprimeCliente(nome, sexo, idade, numCasa, rua, bairro, cidade,  telefone, email, peso, altura, mensalidade, dataPagamento);

	deletarCliente(c1);
	deletarCliente(c2);
	deletarCliente(c3);
	return 0;
}
