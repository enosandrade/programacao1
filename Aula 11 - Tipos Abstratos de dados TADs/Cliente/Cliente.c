#include"Cliente.h"
#include <string.h>

struct cliente{
	char nome[30];
	char sexo;
	int idade;
	
	int numCasa;
	char rua[20];
	char bairro[30];
	char cidade[30];
	

	char telefone[15];
	char email[20];
	
	
	float peso;
	float altura;
	float mensalidade;
	char dataPagamento[20];
};

Cliente *criarCliente(){
		
	Cliente *c = (Cliente*) malloc(sizeof(Cliente));

	if(c != NULL){
		
		printf("\n\n========== Adicionar cliente ==========\n\n");
		fflush(stdin);
		printf("Nome: ");
		gets(c->nome);
		printf("Sexo: ");
		scanf("%s",&c->sexo);
		printf("Idade: ");
		scanf("%d",&c->idade);
		printf("Altura: ");
		scanf("%f",&c->altura);
		printf("Peso: ");
		scanf("%f",&c->peso);
		printf("Rua: ");
		fflush(stdin);
		gets(c->rua );
		printf("Bairro: ");
		gets(c->bairro );
		printf("Cidade: ");
		gets(c->cidade );
		printf("N�mero da casa: ");
		scanf("%d",&c->numCasa);
		printf("Telefone: ");
		fflush(stdin);
		gets(c->telefone);
		printf("Email: ");
		gets(c->email);
		printf("Valor da mensalidade: ");
		scanf("%f",&c->mensalidade);
		fflush(stdin);
		printf("Data de pagamento: ");
		gets(c->dataPagamento);
	}

	return c;
}

void deletarCliente(Cliente *c){
	free(c);
}


void lerDadosCliente(Cliente *c, char *nome, char *sexo, int *idade, int *numCasa, char *rua, char *bairro, char *cidade,
	char *telefone, char *email, float *peso, float *altura, float *mensalidade, char *dataPagamento){
	
	strcpy(nome, c->nome);
	*sexo = c->sexo;
	*idade = c->idade;
	*numCasa = c->numCasa;
	strcpy(rua, c->rua);
	strcpy(bairro, c->bairro);
	strcpy(cidade, c->cidade);
	strcpy(telefone, c->telefone);
	strcpy(email, c->email);
	*peso = c->peso;
	*altura = c->altura;
	*mensalidade = c->mensalidade;
	strcpy(dataPagamento, c->dataPagamento);
}


void escreveDadosCliente(Cliente *c){
		
	printf("\n\n========== Alterar Cliente ==========\n\n");
	fflush(stdin);
	printf("Nome: ");
	gets(c->nome);
	printf("Sexo: ");
	scanf("%c",&c->sexo);
	printf("Idade: ");
	scanf("%d",&c->idade);
	printf("Altura: ");
	scanf("%f",&c->altura);
	printf("Peso: ");
	scanf("%f",&c->peso);
	printf("Rua: ");
	fflush(stdin);
	gets(c->rua );
	printf("Bairro: ");
	gets(c->bairro );
	printf("Cidade: ");
	gets(c->cidade );
	printf("N�mero da casa: ");
	scanf("%d",&c->numCasa);
	printf("Telefone: ");
	fflush(stdin);
	gets(c->telefone);
	printf("Email: ");
	gets(c->email);
	printf("Valor da mensalidade: ");
	scanf("%f",&c->mensalidade);
	fflush(stdin);
	printf("Data de pagamento: ");
	gets(c->dataPagamento);
}
