#include<stdio.h>
#include<stdlib.h> // Pre-definida pelo C

typedef struct cliente Cliente;

Cliente *criarCliente();


void deletarCliente(Cliente *c);

void lerDadosCliente(Cliente *c, char *nome, char *sexo,int *idade, int *numCasa, char *rua, char *bairro, char *cidade,
	char *telefone, char *email, float *peso, float *altura, float *mensalidade, char *dataPagamento);

void escreveDadosCliente(Cliente *c);


