#include "Paciente.h"
#include <string.h>

struct paciente{
	char nome[30];
	char sexo;
	int idade;
	char problemaSaude[30];
	int numCasa;
	char rua[30];
	char bairro[30];
	char cidade[30];
	char telefone[15];
	char email[20];
	float imc;
	float valorInternacao;
	char dataPagamento[15];
};

Paciente *criarPaciente(){

	Paciente *p = (Paciente *) malloc(sizeof(Paciente));
	if(p != NULL){
		printf("/n/n=============== Criar Paciente ===============\n\n");
		fflush(stdin);
		printf("Nome: ");
		gets(p->nome);
		printf("Sexo: ");
		scanf("%c",&p->sexo);
		printf("Idade: ");
		scanf("%d",&p->idade);
		printf("Problema de sa�de: ");
		fflush(stdin);
		gets(p->problemaSaude);
		printf("N�mero da casa: ");
		scanf("%d",&p->numCasa);
		printf("Rua: ");
		fflush(stdin);
		gets(p->rua);
		printf("Bairro: ");
		gets(p->bairro);
		printf("Cidade: ");
		gets(p->cidade);
		printf("Telefone: ");
		gets(p->telefone);
		printf("IMC: ");
		scanf("%f",&p->imc);
		printf("Email: ");
		fflush(stdin);
		gets(p->email);
		printf("Valor da interna��o: ");
		scanf("%c",&p->valorInternacao);
		printf("Data de pagamento: ");
		fflush(stdin);
		gets(p->dataPagamento);
	}
	
	return p;	
}

void deletarPaciente(Paciente *p){
	free(p);
}
void lerDadosPaciente(Paciente *p, char *nome, char *sexo, int *idade, char *problemaSaude, int *numCasa, char *rua,
	char *bairro, char *cidade, char *telefone, char *email, float *imc, float *valorInternacao, char *dataPagamento){
		
	strcpy(nome, p->nome);
	*sexo = p->sexo;
	*idade = p->idade;
	strcpy(problemaSaude, p->problemaSaude);
	*numCasa = p->numCasa;
	strcpy(rua, p->rua);
	strcpy(bairro, p->bairro);
	strcpy(cidade, p->cidade);
	strcpy(telefone, p->telefone);
	strcpy(email, p->email);
	*imc = p->imc;
	*valorInternacao = p->valorInternacao;
	strcpy(dataPagamento, p->dataPagamento);
}

void escreverDadosPaciente(Paciente *p){
	
	printf("/n/n=============== Editar paciente ===============\n\n");
	fflush(stdin);
	printf("Nome: ");
	gets(p->nome);
	printf("Sexo: ");
	scanf("%c",&p->sexo);
	printf("Idade: ");
	scanf("%d",&p->idade);
	printf("Problema de sa�de: ");
	fflush(stdin);
	gets(p->problemaSaude);
	printf("N�mero da casa: ");
	scanf("%d",&p->numCasa);
	printf("Rua: ");
	fflush(stdin);
	gets(p->rua);
	printf("Bairro: ");
	gets(p->bairro);
	printf("Cidade: ");
	gets(p->cidade);
	printf("Telefone: ");
	gets(p->telefone);
	printf("IMC: ");
	scanf("%f",&p->imc);
	printf("Email: ");
	fflush(stdin);
	gets(p->email);
	printf("Valor da interna��o: ");
	scanf("%c",&p->valorInternacao);
	printf("Data de pagamento: ");
	fflush(stdin);
	gets(p->dataPagamento);		
}
