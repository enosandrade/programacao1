#include<stdio.h>
#include<stdlib.h>

typedef struct paciente Paciente;

Paciente *criarPaciente();
	
void deletarPaciente(Paciente *p);

void lerDadosPaciente(Paciente *p, char *nome, char *sexo, int *idade, char *problemaSaude, int *numCasa, char *rua,
	char *bairro, char *cidade, char *telefone, char *email, float *imc, float *valorInternacao, char *dataPagamento);

void escreverDadosPaciente(Paciente *p);


