#include "Paciente.h"
#include <locale.h>

void imprimePaciente(char* nome, char sexo, int idade, char* problemaSaude, int numCasa, char* rua, char* bairro, 
	char* cidade, char* telefone, char* email, float imc, float valorInternacao, char* dataPagamento){
		
	printf("=============== Paciente ===============\n\n");
	printf("Nome: %s\n", nome);
	printf("Sexo: %s\n", sexo == 'F'? "Feminino": "Masculino");
	printf("Idade: %d\n", idade);
	printf("Problema de sa�de: %s\n", problemaSaude);
	printf("N�mero da casa: %d\n", numCasa);
	printf("Rua: %s\n", rua);
	printf("Bairro: %s\n", bairro);
	printf("Cidade: %s\n", cidade);
	printf("Telefone: %s\n", telefone);
	printf("IMC: %.2f\n", imc);
	printf("Email: %s\n", email);
	printf("IMC: %.2f\n", imc);
	printf("Valor da interna��o: %.2f\n", valorInternacao);
	printf("Data de pagamento: %s\n",dataPagamento);
	printf("\n");
}

int main(){
	setlocale(LC_ALL,"Portuguese");
	Paciente *p1, *p2, *p3;
	
	char nome[30];
	char sexo;
	int idade;
	char problemaSaude[30];
	int numCasa;
	char rua[30];
	char bairro[30];
	char cidade[30];
	char telefone[15];
	char email[20];
	float imc;
	float valorInternacao;
	char dataPagamento[15];
	
	

	p1 = criarPaciente();
	p2 = criarPaciente();
	p3 = criarPaciente();

	lerDadosPaciente(p1, &nome, &sexo, &idade,&problemaSaude, &numCasa, &rua, &bairro, &cidade, &telefone, &email, &imc, &valorInternacao, &dataPagamento);
	imprimePaciente(nome, sexo, idade, problemaSaude, numCasa, rua, bairro, cidade,  telefone, email, imc, valorInternacao, dataPagamento);

	lerDadosPaciente(p2, &nome, &sexo, &idade,&problemaSaude, &numCasa, &rua, &bairro, &cidade, &telefone, &email, &imc, &valorInternacao, &dataPagamento);
	imprimePaciente(nome, sexo, idade, problemaSaude, numCasa, rua, bairro, cidade,  telefone, email, imc, valorInternacao, dataPagamento);

	lerDadosPaciente(p3, &nome, &sexo, &idade,&problemaSaude, &numCasa, &rua, &bairro, &cidade, &telefone, &email, &imc, &valorInternacao, &dataPagamento);
	imprimePaciente(nome, sexo, idade, problemaSaude, numCasa, rua, bairro, cidade,  telefone, email, imc, valorInternacao, dataPagamento);

	escreverDadosPaciente(p1);
	lerDadosPaciente(p1, &nome, &sexo, &idade,&problemaSaude, &numCasa, &rua, &bairro, &cidade, &telefone, &email, &imc, &valorInternacao, &dataPagamento);
	imprimePaciente(nome, sexo, idade, problemaSaude, numCasa, rua, bairro, cidade,  telefone, email, imc, valorInternacao, dataPagamento);

	deletarPaciente(p1);
	deletarPaciente(p2);
	deletarPaciente(p3);
	return 0;
}
