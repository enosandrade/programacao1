#include <stdio.h>
#include <stdlib.h>
#include <locale.h>

int pot(int base, int expo){
    if(expo == 0){
        return 1;
    }else if(expo>0){
        return base * pot(base,expo-1);
    }
}


int main(){
	int base, expo;
    printf("Base: ");
    scanf("%d", &base);
    printf("Expoente: ");
    scanf("%d", &expo);
    printf("\n%d elevado a %d é igual a %d", base,expo, pot(base,expo));
    
	return 0;
}
