#include <stdio.h>
#include <stdlib.h>
#include <locale.h>

int fib(int x){
   if(x==1 || x==2)
       return 1;
   else
       return fib(x-1) + fib(x-2); 
}


int main(){
	int x,c;
    printf("Número: ");
    scanf("%d", &x);
    for(c =1;c <=x;c++){
        printf(" %d ", fib(c));
    }
	return 0;
}

