#include <stdio.h>
#include <stdlib.h>
#include <locale.h>

int mdc(int x, int y){
    if(y == 0){
        return x;
    }else{
        return mdc(x,x%y);
    }
}


int main(){
	int x, y;
    printf("X: ");
    scanf("%d", &x);
    printf("Y: ");
    scanf("%d", &y);
    printf("O MDC é %d ", mdc(x,y));
    
	return 0;
}
