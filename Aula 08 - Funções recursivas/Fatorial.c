#include <stdio.h>
#include <stdlib.h>
#include <locale.h>

int fat(int x){
    if(x == 0){
        return 1;
    }else{
        return x * fat(x-1);
    }
}


int main(){
	int x;
    printf("Número: ");
    scanf("%d", &x);
    printf("%d! é %d ", x,fat(x));
    
	return 0;
}
