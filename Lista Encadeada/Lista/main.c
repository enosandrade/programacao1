#include "Lista.h"
FILE *f;
char caminho[]="estudantes.bin";

int main()
{
    setlocale(LC_ALL,"Portuguese");
    List* l = createList();

    DataNode data;

    data.id = 5;
    push(l, data);

    data.id = 9;
    push(l, data);

    data.id = 10;
    push(l, data);

    data.id = 11;
    push(l, data);


    data.id = 100;
    append(l, data);


    FILE *f;
    writeList(f,"arquivo.bin",l);

    List* list = readList(f,"arquivo.bin");

    printList(list);
}
