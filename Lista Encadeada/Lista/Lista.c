#include "Lista.h"

List* readList(FILE *f, char* caminho){
    fopen(caminho, "rb");

    List* l;
    fread(l,sizeof(List),1,f);
    return l;
}

void writeList(FILE*f, char* caminho, List* list){
    fopen(caminho, "ab");

    Node* node = list->head;

    while(node != NULL){
        fwrite(node,sizeof(Node),1,f);
        node = node->next;
    }
    fclose(f);

}
List* createList(){
    List* list = (List *) malloc(sizeof(List));
    list->size = 0;
    list->head = NULL;

    return list;
}

void push(List* list, DataNode data){
    Node* node = (Node*) malloc(sizeof(Node));
    node->data = data;
    node->next = list->head;
    list->head = node;
    list->size++;

}

void printList(List* list){
    printf("==================== Lista ====================\n\n");

    if(isEmpty(list)){
        printf("Lista vazia\n");
        return;
    }

    Node *pointer = list->head;
    int index = 0;

    while(pointer != NULL){
         printf("N�: %d, Conte�do: %d\n", index, pointer->data.id);
         pointer = pointer->next;
         index++;
    }

}

void pop(List* list){
    if(!isEmpty(list)){
        Node* pointer = list->head;
        list->head = pointer->next;
        free(pointer);
        list->size--;
    }

}

bool isEmpty(List* list){
    return (list == NULL) || (list->head == NULL);
}

Node* atPos(List* list, int index){
    if(index >= 0 && index < list->size){
        Node* node = list->head;

        int i;
        for(i = 0; i < index; i++)
            node = node->next;
        return node;
    }
    printf("\nIndex Inv�lido\n");

    return NULL;
}

int indexOf(List* list, Node* node){
    if(node != NULL){
        Node* pointer = list->head;

        int index = 0;

        while(pointer != node && pointer != NULL){
            pointer = pointer->next;
            index++;
        }
        if(pointer != NULL)
            return index;
    }
    printf("N� n�o pertencente a lista\n");
    return -1;

}
void erase(List* list, int index){
    if(index == 0){
        pop(list);
    }
    else{
        Node* current = atPos(list, index);

        if(current != NULL){
            Node* previous = atPos(list, index - 1);

            previous->next = current->next;
            free(current);
            list->size--;
        }
    }
}

void insert(List* list, DataNode data, int index){
    if(index == 0){
        push(list, data);
    }else{
        Node* current = atPos(list, index);
        if(current != NULL){
            Node* previous = atPos(list, index - 1);

            Node* newNode = (Node*)malloc(sizeof(Node));
            newNode->data = data;

            previous->next = newNode;
            newNode->next = current;

            list->size++;
        }
    }
}
void append(List* list, DataNode data){
    if(isEmpty(list))
        push(list, data);
    else{
        Node* node =(Node*)  malloc(sizeof(Node));
        node->data = data;

        Node* previous = list->head;

        while(previous->next != NULL){
            previous = previous->next;
        }
        previous->next = node;
        node->next = NULL;
        list->size++;

    }
}
void freelist(List* list){
    if(list != NULL){

        Node* node;
        while(list->head != NULL){
            node = list->head;
            list->head = node->next;
            free(node);
        }
        list->size = 0;
        free(list);
    }
}
int size(List* list){
    return list->size;
}
