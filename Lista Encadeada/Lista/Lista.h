#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#include <stdbool.h>

typedef struct dataNode{
    int id;
}DataNode;

typedef struct node{
    DataNode data;
    struct node *next;
}Node;

typedef struct list{
    int size;
    Node *head;

}List;

List* createList();
void push(List* list, DataNode data);
void printList(List* list);
void pop(List* list);
bool isEmpty(List* list);
Node* atPos(List* list, int index);
indexOf(List* list, Node* node);
void erase(List* list, int index);
void insert(List* list, DataNode data, int index);
void append(List* list, DataNode data);
void freelist(List* list);
int size(List* list);
