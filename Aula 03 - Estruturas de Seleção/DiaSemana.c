#include <stdio.h>
#include <stdlib.h>
#include <locale.h>

int main() {
	setlocale(LC_ALL,"Portuguese");
	int dia; 
	printf("\n========================== Dia da semana ==========================\n\n");
	printf("Informe um n�mero entre 1 e 7: ");
	scanf("%d", &dia);
	
	switch(dia){
		case 1:
			printf("\nDOMINGO!");
			break;
		case 2:
			printf("\nSEGUNDA-FEIRA!");
			break;
		case 3:
			printf("\nTER�A-FEIRA!");
			break;
		case 4:
			printf("\nQUARTA-FEIRA!");
			break;	
		case 5:
			printf("\nQUINTA-FEIRA!");
			break;
		case 6:
			printf("\nSEXTA-FEIRA!");
			break;
		case 7:
			printf("\nS�BADO!");
			break;
		default:
			printf("\nDIA INV�LIDO!");
		
	}
	return 0;
}
