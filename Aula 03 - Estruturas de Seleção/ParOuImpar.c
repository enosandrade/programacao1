#include <stdio.h>
#include <stdlib.h>
#include <locale.h>

/* run this program using the console pauser or add your own getch, system("pause") or input loop */

int main(int argc, char *argv[]) {
	setlocale(LC_ALL, "Portuguese");
	
	int numero, continuar;
	continuar = 1;
	while(continuar){
		printf("\n========================== Par ou �mpar ==========================\n\n");
		printf("Digite um n�mero: ");
		scanf("%d", &numero);
	
		if(numero%2 == 0){
		printf("\n%d � PAR!\n", numero);
		}
		else{
			printf("\n%d � �MPAR!\n", numero);
		}
		printf("\nDeseja continuar analizando? ['1' para SIM ou '0' para N�O]: ");
		scanf("%d", &continuar);
		
		while(continuar != 0 && continuar != 1){
			printf("\nOp��o INV�LIDA!\n");
			printf("\nDeseja continuar analizando? ['1' para SIM ou '0' para N�O]: ");
			scanf("%d", &continuar);
			
		}
		if(!continuar){
			printf("\nPrograma finalizado!\n\n");
		}
		
	}
	
	system("pause");
	
	return 0;
}
