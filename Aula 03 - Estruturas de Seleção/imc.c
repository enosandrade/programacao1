#include <stdio.h>
#include <stdlib.h>
#include <locale.h>


int main(int argc, char *argv[]) {
	setlocale(LC_ALL, "Portuguese");
	char nome[60], sexo;
	float peso, altura, imc;
	
	printf("\n========================== IMC ==========================\n\n");
	printf("Nome completo: ");
	gets(nome);
	printf("Sexo: ");
	scanf("%c", &sexo);
	printf("Peso: ");
	scanf("%f", &peso);
	printf("Altura: ");
	scanf("%f", &altura);
	
	//Relatorio
		printf("\n========================== Resultados ==========================\n\n");
		printf("Nome: %s", nome);
		printf("\nSexo: %c", sexo);
		imc =  peso/(altura * altura);
		printf("\nIMC: %.2f",imc);
		
		if(imc <= 18.5){
			printf("\nSitua��o: Abaixo do peso ideal");
		}else if(imc > 18.5 && imc <25){
			printf("\nSitua��o: Peso ideal");
		}else if(imc >= 25 && imc <30){
			printf("\nSitua��o: Excesso de peso");
		} else if(imc >=30 && imc <35){
			printf("\nSitua��o: Obesidade leve");
		}else if(imc >=35 && imc <40){
			printf("\nSitua��o: Obesidade severa");
		}else{
			printf("\nSitua��o: Obesidade m�rbida");
		}
		
	return 0;
}
