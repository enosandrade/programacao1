#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
 
int main(int argc, char *argv[]) {
    setlocale(LC_ALL,"Portuguese");
     
    struct contato{
        int telefone;
        char email[30];
        char redeSocial[50];
    };
    
    struct endereco{
      char pais[30], estado[30], cidade[30], bairro[30], rua[30], complemento[100];
      int numero;
    };
     
    struct professor{
        char nome[30], sexo, disciplina[30];
        int dataNasc;
        struct endereco endereco;
        struct contato contato;
    };
     
    struct professor professor[2];
     
    int i;
    for(i = 0; i < 2; i++){
        setbuf(stdin,NULL);
        printf("\n===============\nProfessor %d:\n===============\n", i+1);
        printf("Nome: ");
        gets(professor[i].nome);
        
        printf("Sexo: ");
        scanf("%c", &professor[i].sexo);
        
        printf("Data de nascimento: ");
        scanf("%i", &professor[i].dataNasc);
        
        setbuf(stdin,NULL);
        
        printf("Disciplina: ");
        gets(professor[i].disciplina);
        
        printf("E-mail: ");
        gets(professor[i].contato.email);
        
        printf("Telefone: ");
        scanf("%d", &professor[i].contato.telefone);
        
        setbuf(stdin,NULL);
        
        printf("Rede Social: ");
        gets(professor[i].contato.redeSocial);
        
        printf("País: ");
        gets(professor[i].endereco.pais);
        
        printf("Estado: ");
        gets(professor[i].endereco.estado);
        
        printf("Cidade: ");
        gets(professor[i].endereco.cidade);
        
        printf("Bairro: ");
        gets(professor[i].endereco.bairro);
        
        printf("Rua: ");
        gets(professor[i].endereco.rua);
        
        printf("Número: ");
        scanf("%d", &professor[i].endereco.numero);
        
        setbuf(stdin,NULL);
        
        printf("Complemento:");
        gets(professor[i].endereco.complemento);
        system("cls");
    }
    
    for(i = 0; i<2; i++){
        printf("\nProfessor %d:\n\n", i+1);
        printf("Nome: %s\n", professor[i].nome);
        printf("Sexo: %c\n", professor[i].sexo);
        printf("Data de nascimento: %d\n", professor[i].sexo);
        printf("Disciplina: %s\n", professor[i].disciplina);
        printf("\n\nEndereço:\n\n");
        printf("País: %s\n", professor[i].endereco.pais);
        printf("Estado: %s\n", professor[i].endereco.estado);
        printf("Cidade: %s\n", professor[i].endereco.cidade);
        printf("Bairro: %s\n", professor[i].endereco.bairro);
        printf("Rua: %s\n", professor[i].endereco.rua);
        printf("Número: %i\n", professor[i].endereco.numero);
        printf("Complemento: %s\n", professor[i].endereco.complemento);
        printf("\n\nContato:\n\n");
        printf("E-mail: %s\n", professor[i].contato.email);
        printf("Telefone: %d\n", professor[i].contato.telefone);
        printf("Rede Social: %s\n", professor[i].contato.redeSocial);
    }
     
    return 0;
}