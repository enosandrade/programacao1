#include <stdio.h>
#include <locale.h>


int main()
{
    int n, i, fatorial;
    i = 1;
    fatorial = 1;
    
    setlocale(LC_ALL, "Portuguese");
    printf("=============== Fatorial ================\n\n");
    
    printf("Informe o número: ");
    scanf("%d", &n);
    
    if(n < 0){
        printf("\nApenas números positivos.");
    }else{
        while(i <= n){
            fatorial*=i;
            i++;
        }
        printf("\n%d! = %d", n, fatorial);
    }

    return 0;
}
