#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
int main()
{
    setlocale(LC_ALL, "Portuguese");
    
    int linha, coluna, prof, a[2][3][4], b[2][3][4], c[2][3][4];
    
    printf("Preencha a matriz A (linha, coluna, profundidade): \n\n");
	for(prof = 0; prof < 4; prof++){  //Preenche a matriz A
		for(linha = 0; linha < 2; linha++){
		    for(coluna = 0; coluna <3; coluna++){
		        printf("(%d, %d, %d): ",linha+1, coluna+1, prof+1);
			    scanf("%d",&a[linha][coluna][prof]);    
		    }
			
		}
	}
	system("cls");
		
    printf("Preencha a matriz B (linha, coluna, profundidade): \n\n");
	for(prof = 0; prof < 4; prof++){  //Preenche a matriz B
		for(linha = 0; linha < 2; linha++){
		    for(coluna = 0; coluna <3; coluna++){
		        printf("(%d, %d, %d): ",linha+1, coluna+1, prof+1);
			    scanf("%d",&b[linha][coluna][prof]);    
		    }
			
		}
	}
		
    printf("Preencha a matriz B (linha, coluna, profundidade): \n\n");
	for(prof = 0; prof < 4; prof++){  
		for(linha = 0; linha < 2; linha++){
		    for(coluna = 0; coluna <3; coluna++){
		        c[linha][coluna][prof] = a[linha][coluna][prof] + b[linha][coluna][prof];
		    }
			
		}
	}
	system("cls");

	for(prof = 0; prof < 4; prof++){ 
	    printf("\nMatriz para a profundidade: %d\n\n", prof+1);
	    for(linha = 0; linha < 2; linha++){
	        for(coluna = 0; coluna < 3; coluna++){
	            printf(" %d ", c[linha][coluna][prof]);
	        }
	        printf("\n");
	    }
	}
	
    return 0;
}
