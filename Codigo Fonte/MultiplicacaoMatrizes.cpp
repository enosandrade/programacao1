#include <stdio.h>
int main(){
	// linhas e colunas de a e b
	int l_a, c_a, l_b, c_b, l_c, c_c;
	int L, C;

	printf("Digite a quantidade de linhas de A: ");
	scanf("%d", &l_a);

	printf("Digite a quantidade de colunas de A: ");
	scanf("%d", &c_a);

	printf("Digite a quantidade de colunas de B: ");
	scanf("%d", &c_b);

	l_b=c_a;
	l_c=l_a;
	c_c=c_b;

	int a[l_a][c_a];
	int b[l_b][c_b]; // Linhas de b = colunas de a
	int c[l_c][c_c]; // Linhas de a x colunas de b

    printf("\nPREENCHENDO AO MATRIZ A\n");
	for(L=0; L<l_a; L++){
		for(C=0; C<c_a; C++){
			printf("Informe A[%d %d]: ", L, C);
			scanf("%d", &a[L][C]);
		}
	}

	printf("\nPREENCHENDO AO MATRIZ B\n");
	for(L=0; L<l_b; L++){
		for(C=0; C<c_b; C++){
			printf("Informe B[%d %d]: ", L, C);
			scanf("%d", &b[L][C]);
		}
	}

    //Multiplicacao de duas matrizes
	for(int i=0; i<l_a; i++){ // percorre as linhas de a
		for(int j=0; j<c_b; j++){ // percorre as colunas de b
			int aux=0;
			for(int v=0; v<c_a; v++){ // faz a variação para a multiplicaçao
				aux +=  (a[i][v]*b[v][j]);
			}
			c[i][j]=aux;
		}
	}

    printf("\nRESULTADO DE A x B\n");
	for(L=0; L<l_c; L++){
		for(C=0; C<c_c; C++){
			printf("%d \t", c[L][C]);
		}
		printf("\n");
	}
	return 0;
}
