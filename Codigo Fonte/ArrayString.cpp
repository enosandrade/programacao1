#include <stdio.h>

typedef char *string;
 
int main(void)
{
  string strgs[5];  //Array de  5 strings
  int i;
 
  strgs[0] = "Primeira string";
  strgs[1] = "Segunda string";
  strgs[2] = "Terceira string";
  strgs[3] = "Aqui esta a quarta string";
  strgs[4] = "Finalmente, esta e a quinta string";
 
  for(i = 0;i < 5;++i)
    puts(strgs[i]);
 
  return 0;
}
