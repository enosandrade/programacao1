#include <stdio.h>

struct endereco{
	char pais[], estado[], cidade[], bairro[], rua[];
	int numero, cep;
};

struct contato{
	char email[], celular[];
};

struct professor{
	char nome[], sexo[];
	int idade;
	float salario;
	struct endereco end;
	struct contato cont;
};

int main(){
	struct professor profs[10];
	
	for(int i= 0; i<10; i++){
		printf("Nome do professor: ");
		gets(profs[i].nome);
		printf("Sexo: ");
		gets(profs[i].sexo);		
		printf("Pais: ");
		gets(profs[i].end.pais);
		printf("Estado: ");
		gets(profs[i].end.estado);
		printf("Cidade: ");
		gets(profs[i].end.cidade);
		printf("Bairro: ");
		gets(profs[i].end.bairro);		
		printf("E-mail: ");
		gets(profs[i].cont.email);
		printf("Celular: ");
		gets(profs[i].cont.celular);
		
		printf("CEP: ");
		scanf("%d", &profs[i].end.cep);
		printf("N�mero da casa: ");
		scanf("%d", &profs[i].end.numero);
		printf("Idade: ");
		scanf("%d", &profs[i].idade);
		printf("Salario: ");
		scanf("%f", &profs[i].salario);
	}
	
	
	return 0;
}
