// ABRINDO E FECHANDO UM ARQUIVO BINARIO LEITURA
// Armazenamento em mem�riao permanente

#include<stdio.h>
#include<stdlib.h>

int main(){
	
	// Ponteiro para um arquivo 
	FILE *f;
	
	// Abrindo um arquivo em modo escrita w
	f = fopen("C:\\Arquivos\\array.txt", "rb"); /*Nesse caminho deve existeir
	um arquivo de texto criado ap�s a execuss�o desse programa*/
	
	if(f==NULL){
		printf("Erro ao criar o arquivo");
		exit(1);
	}
	
	char s[20];
	double x;
	int v[9];
	
	fread(s, sizeof(char), 20, f);
	fread(&x, sizeof(double), 1, f);
	fread(v, sizeof(int), 9, f);
	
	printf("%s, %f \n\n", s, x);	
	printf("%d, %d, %d, %d, %d", v[0], v[1], v[2], v[3], v[4]);
	
	
	// Fecha o arquivo
	fclose(f);
	
	
	return 0;
}
