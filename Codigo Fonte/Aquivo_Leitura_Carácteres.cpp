// LEITURA DE CAR�CTERES EM UM ARQUIVO


#include<stdio.h>
#include<stdlib.h>
#include<string.h> // Biblioteca para trabalhar com texto

int main(){
	
	// 1 - Ponteiro para um arquivo 
	FILE *f;
	
	// 2 - Abrindo um arquivo em modo leitura
	f = fopen("C:\\Arquivos\\texto.txt", "r"); 
	
	if(f==NULL){
		printf("Erro ao criar o arquivo");
		exit(1);
	}
	
	// Lendo do arquivo que foi aberto no modo escrita
	char c = fgetc(f); // Essa fun��o ler letra por letra.
	
	while(c!=EOF){
		printf("%c", c);
		c = fgetc(f);
	}
	
	printf("\nFim\n");
	
	// 3 -  Fecha o arquivo
	fclose(f);
	
	
	return 0;
}
