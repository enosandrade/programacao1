// ESCRITA DE STRINGS INTEIRAS EM UM ARQUIVO


#include<stdio.h>
#include<stdlib.h>

int main(){
	
	// 1 - Ponteiro para um arquivo 
	FILE *f;
	
	// 2 - Abrindo um arquivo
	f = fopen("C:\\Arquivos\\texto2.txt", "w"); 
	
	if(f==NULL){
		printf("Erro ao criar o arquivo");
		exit(1);
	}
	
	// Escrever dentro do arquivo que foi aberto no modo escrita
	char text[] = "GRAVA��O DE STRING INTEIRA E DE UMA VEZ";
	int i;
	
	int r = fputs(text, f); // Essa fun��o escreve a string inteira.
	
	if(r==EOF)
		printf("Erro na grava��o no arquivo");
	
	fputs("Helo World", f);
	fputs("Turma Boa", f);
	
	fputs("Turma de Programa��o", f);
	fputs("\n", f);	
	fputs("Que Estuda, passa", f);
	
	
	// 3 -  Fecha o arquivo
	fclose(f);
	
	
	return 0;
}
