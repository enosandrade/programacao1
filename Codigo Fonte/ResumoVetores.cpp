
main(){
	/*
	RESUMO SOBRE VETORES
	Conjunto de variaveis de mesmo tipo declaradas de uma vez so
	
	tipo_dado nome_array[numero_linhas][numero_colunas]

	*/
		
	if(1){ // VETOR UNI-DIMENSIONAL
		int v_nome[6]= {1, 3, 5, 6, 7, 8}; // Vetor inicializado
		int i;							   // indices 0-1-2-3-4-5
		
		// atribui��o
		v_nome[5] = 10; // Atribui 10 na variavel de indice 5
						// trocando o valor 8 por 10
		//utiliza��o
		printf("%d", v_nome[3]); // Exibe o valor 6 da variavel
								 // de indice 4
		//percorrendo o vetor
		for(i=0; i<6; i++) // i- indice generico
			v_nome[i]=i+3;	// Atribuicao na variavel v_nome[i]		
		
		for(i=0; i<6; i++) // i- indice generico
			printf("%d", v_nome[i]); // Utiliza��o de v_nome[i]
	}
	
	if(1){ //VETOR BI-DIMENSIONAL
		
		// Vetor inicializado
		float m[5][4] = {
					{0.0, 0.1, 0.2, 0.3}, 
				    {1.0, 1.1, 1.2, 1.3}, 
				    {2.0, 2.1, 2.2, 2.3}, 
				    {3.0, 3.1, 3.2, 3.3},
				    {4.0, 4.1, 4.2, 4.3}
										};
		int L, C; // numero de linas e colunas
		
		//percorre a matriz, vetor bi-dimensional	
		for(L=0; L<5; L++){ // Anda linha por linha
			printf("\n");
			for(C=0; C<4; C++)// Anda coluna por coluna
				printf("%.1f ", m[L][C]); // mostra o valor da variavel			
		}
		
		//percorre a matriz, vetor bi-dimensional	
		for(L=0; L<5; L++){ // Anda linha por linha			
			for(C=0; C<4; C++) {// Anda coluna por coluna
				printf("Informe o [%d , %d]", L, C); // mostra o valor da variavel	
				scanf("%f", &m[L][C]); // Atribui novos valores nas variaveis de m
			}
		}
	}
	
	if(1) {// VETOR TRI-DIMENSIONAL
	
	}
	
	if(1) {// VETOR N-DIMENSIONAL
		
		int vet[5]; // 1 dimens�o
		float mat[5][5]; // 2 dimens�es
		double cub[5][5][5]; // 3 dimens�es
		int X[5][5][5][5]; // 4 dimens�es
		/*
		OBS - O idice mais a direita varia mais rapidamente
		*/

	}
}
