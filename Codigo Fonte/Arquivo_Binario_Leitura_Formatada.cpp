// ABRINDO E FECHANDO UM ARQUIVO BINARIO LEITURA FORMATADA
// Armazenamento em mem�riao permanente

#include<stdio.h>
#include<stdlib.h>

int main(){
	
	// Ponteiro para um arquivo 
	FILE *f;
	
	// Abrindo um arquivo em modo escrita w
	f = fopen("C:\\Arquivos\\alun.txt", "r"); /*Nesse caminho deve existeir
	um arquivo de texto criado ap�s a execuss�o desse programa*/
	
	if(f==NULL){
		printf("Erro ao criar o arquivo");
		exit(1);
	}
	
	char texto[20], nome[20];
	int i;
	double m;
		
	fscanf(f, "%s %s", texto, nome);
	printf("%s %s \n", texto, nome);
	fscanf(f, "%s %d", texto, &i);
	printf("%s %d \n", texto, i);
	fscanf(f, "%s %f", texto, &m);
	printf("%s %.2f \n", texto, m);
	
	
	// Fecha o arquivo
	fclose(f);
	
	
	return 0;
}
