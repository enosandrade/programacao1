// PASSAGEM DE MATRIZ POR PAR�METRO
#include <stdio.h>

void povoa_matriz(int m[3][2]){
	int i, j;
	
	for(i=0; i<3; i++){
		
		for(j=0; j<2; j++){
			printf("Digite o elemento %d %d: ", i, j);
			scanf("%d", &m[i][j]);
		}
	}
}

void imprime_matriz(int m[3][2]){
	int i, j;
	for(i=0; i<3; i++){
		
		for(j=0; j<2; j++){
			printf("%d ", m[i][j]);
		}
		printf("\n");
	}
}

int main(){
	int mat1[3][2];
	int mat[3][2];
	
	povoa_matriz(mat);
	povoa_matriz(mat1);
	imprime_matriz(mat);
	
	return 0;
}


