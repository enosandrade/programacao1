// ABRINDO E FECHANDO UM ARQUIVO 
// Armazenamento em mem�riao permanente

#include<stdio.h>
#include<stdlib.h>

/*
Todo arquivo tem uma extens�o que serve para o SO decidir sobre
qual programa ir� abrir aquele arquivo e nada mais

C permite trabalhar com arquivos de texto e bin�rios
*/
int main(){
	
	// Ponteiro para um arquivo 
	FILE *f;
	
	// Abrindo um arquivo em modo escrita w
	f = fopen("C:\\Arquivos\\texto.txt", "w"); /*Nesse caminho deve existeir
	um arquivo de texto criado ap�s a execuss�o desse programa*/
	
	if(f==NULL){
		printf("Erro ao criar o arquivo");
		exit(1);
	}
	
	/*
	Faz a lista de tarefas dentro do arquivo
	*/
	
	// Fecha o arquivo
	fclose(f);
	
	
	return 0;
}
