// ABRINDO E FECHANDO UM ARQUIVO BINARIO GRAVA��O
// Armazenamento em mem�riao permanente

#include<stdio.h>
#include<stdlib.h>

int main(){
	
	// Ponteiro para um arquivo 
	FILE *f;
	
	// Abrindo um arquivo em modo escrita w
	f = fopen("C:\\Arquivos\\array.txt", "wb"); /*Nesse caminho deve existeir
	um arquivo de texto criado ap�s a execuss�o desse programa*/
	
	if(f==NULL){
		printf("Erro ao criar o arquivo");
		exit(1);
	}
	
	int a[9]={2, 5, 7, 78, 67, 89, 45, 32, 56};
	
	int qtd_gravada = fwrite(a, sizeof(int), 9, f); // Fun��o que grava
	
	if(qtd_gravada != 9){
		printf("Erro na escrita do arquivo");
		exit(1);
	}
	
	double sal = 1435.86;
	char t[40] = "Brincando com arquivos bin�rios";
	fwrite(&sal, sizeof(double), 1, f);
	fwrite(t, sizeof(char), 40, f);	
	
	// Fecha o arquivo
	fclose(f);
	
	
	return 0;
}
