// ACESSO ALEAT�RIO AO ARQUIVO

#include<stdio.h>
#include<stdlib.h>

struct estoque{
	char nome[30];
	int quantidade;
	float preco;
};

int main(){
	FILE *f;
	
	f = fopen("C:\\Arquivos\\aleatorio.txt", "rb");
	
	if(f==NULL){
		printf("Erro na abertura do arquivo");
		exit(1);
	} 
	
	struct estoque p;
	// Move o cursor dentro do arquivo
	fseek(f, 3*sizeof(struct estoque), SEEK_SET);
	
	fread(&p, sizeof(struct estoque), 1, f);
	
	printf("Produdo: %s \n", p.nome);
	printf("Quantidade: %d \n", p.quantidade);
	printf("Valor: %f \n", p.preco);
	
	rewind(f); // Volta para o come�o do arquivo	
	
	fread(&p, sizeof(struct estoque), 1, f);
	
	printf("Produdo: %s \n", p.nome);
	printf("Quantidade: %d \n", p.quantidade);
	printf("Valor: %f \n", p.preco);
	
	
	fclose(f);

}
