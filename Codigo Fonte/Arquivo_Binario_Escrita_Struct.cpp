// ABRINDO E FECHANDO UM ARQUIVO BINARIO GRAVA��O DE STRUCTS 
// Armazenamento em mem�riao permanente

#include<stdio.h>
#include<stdlib.h>

struct aluno{
	char nome[50], endereco[70];
	int idade;
	float media;
};

int main(){
	
	// Ponteiro para um arquivo 
	FILE *f;
	
	// Abrindo um arquivo em modo escrita w
	f = fopen("C:\\Arquivos\\alunos.txt", "wb"); /*Nesse caminho deve existeir
	um arquivo de texto criado ap�s a execuss�o desse programa*/
	
	if(f==NULL){
		printf("Erro ao criar o arquivo");
		exit(1);
	}
	
	struct aluno c = {"Joao da Silva", "Rua da Saudade", 34, 9.9};
	
	int qtd_gravada = fwrite(&c, sizeof(struct aluno), 1, f); // Fun��o que grava
	
	
	
	// Fecha o arquivo
	fclose(f);
	
	
	return 0;
}
