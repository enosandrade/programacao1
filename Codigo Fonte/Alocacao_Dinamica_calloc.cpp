// ALOCA��O DIN�MICA DE MEM�RIA
// calloc

#include<stdlib.h>
#include<stdio.h>

int main(){
	int *p, i, N;
	
	printf("Digite a quantidade de inteiros que precisa armazenar: ");
	scanf("%d", &N);
	
	// Um ponteiro dizendo onde come�a os N n�meros inteiro
	p = (int*) calloc(N, sizeof(int));
	
	// Se n�o tiver mem�ria dispon�vel, vai parar o programa exepcionalmente, for�ado.
	if(p==NULL){
		printf("Nao foi possivel alocar a memoria");
		exit(1); 
		/*exit() � da biblioteca stdlib interrompe a execu��o do programa e fecha 
		todos os arquivos que o programa tenha porventura aberto.  Se o argumento 
		da fun��o for 0, o sistema operacional � informado de que o programa terminou 
		com sucesso; caso contr�rio, o sistema operacional � informado de que o 
		programa terminou de maneira excepcional.*/
	}
	// Coloca valores dentro do bloco de memoria alocado
	for(i=0;i<N;i++){
		p[i]=i+3;
	}
	
	// Imprime os valores do bloco de mem�ria alocado
	for(i=0;i<N;i++){
		printf("%d, ", p[i]);
	}
	
	// lembrando que com aloca��o din�mica de memoria � preciso liberar explicitamente
	free(p);
	
	
	return 0;
}
