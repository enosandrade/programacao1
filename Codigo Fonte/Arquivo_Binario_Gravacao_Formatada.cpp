// ABRINDO E FECHANDO UM ARQUIVO BINARIO GRAVA��O FORMATADA
// Armazenamento em mem�riao permanente

#include<stdio.h>
#include<stdlib.h>

struct aluno{
	char nome[50], endereco[70];
	int idade;
	float media;
};

int main(){
	
	// Ponteiro para um arquivo 
	FILE *f;
	
	// Abrindo um arquivo em modo escrita w
	f = fopen("C:\\Arquivos\\alun.txt", "w"); /*Nesse caminho deve existeir
	um arquivo de texto criado ap�s a execuss�o desse programa*/
	
	if(f==NULL){
		printf("Erro ao criar o arquivo");
		exit(1);
	}
	
	struct aluno a = {"Joao da Silva", "Rua da Saudade", 34, 9.9};
	
	printf("Nome: %s \n", a.nome);
	printf("Endereco: %s \n", a.endereco);
	printf("Idade: %d \n", a.idade);
	printf("Media: %.2f \n", a.media);
	
	
	fprintf(f, "Nome: %s \n Endereco: %s \n Idade: %d \n Media: %.2f \n", a.nome, a.endereco, a.idade, a.media);
	
	// Fecha o arquivo
	fclose(f);
	
	
	return 0;
}
