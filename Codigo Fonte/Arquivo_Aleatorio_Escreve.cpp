// ACESSO ALEATÓRIO AO ARQUIVO

#include<stdio.h>
#include<stdlib.h>

struct estoque{
	char nome[30];
	int quantidade;
	float preco;
};

int main(){
	FILE *f;
	
	f = fopen("C:\\Arquivos\\aleatorio.txt", "wb");
	
	if(f==NULL){
		printf("Erro na abertura do arquivo");
		exit(1);
	}
	
	struct estoque c[5]  = 	{"Arroz", 20, 3.45, 
							 "Leite", 10, 2.56,
							 "Biscoito", 50, 5.05,
							 "Fritas", 40, 6.50, 
							 "Farinha", 23, 2.00};

	fwrite(c, sizeof(struct estoque), 5, f);
	
	fclose(f);

}
