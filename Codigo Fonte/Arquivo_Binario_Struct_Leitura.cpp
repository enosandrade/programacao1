// ABRINDO E FECHANDO UM ARQUIVO BINARIO LEITURA
// Armazenamento em mem�riao permanente

#include<stdio.h>
#include<stdlib.h>

struct aluno{
	char nome[50], endereco[70];
	int idade;
	float media;
};

int main(){
	
	// Ponteiro para um arquivo 
	FILE *f;
	
	// Abrindo um arquivo em modo escrita w
	f = fopen("C:\\Arquivos\\alunos.txt", "rb"); /*Nesse caminho deve existeir
	um arquivo de texto criado ap�s a execuss�o desse programa*/
	
	if(f==NULL){
		printf("Erro ao criar o arquivo");
		exit(1);
	}
	
	struct aluno a;
	
	fread(&a, sizeof(struct aluno), 1, f);
	
	
	printf("Nome: %s \n", a.nome);
	printf("Endereco: %s \n", a.endereco);
	printf("Idade: %d \n", a.idade);
	printf("Media: %.2f \n", a.media);	

	
	
	// Fecha o arquivo
	fclose(f);
	
	
	return 0;
}
