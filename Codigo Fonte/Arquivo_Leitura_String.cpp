// LEITURA DE STRINGS INTEIRAS EM UM ARQUIVO


#include<stdio.h>
#include<stdlib.h>

int main(){
	
	// 1 - Ponteiro para um arquivo 
	FILE *f;
	
	// 2 - Abrindo um arquivo em modo leitura
	f = fopen("C:\\Arquivos\\texto2.txt", "r"); 
	
	if(f==NULL){
		printf("Erro ao criar o arquivo");
		exit(1);
	}
	
	// Lendo string em um arquivo que foi aberto no modo leitura
	char text[20];
	int i;
	
	char *r = fgets(text, 20, f); // Essa fun��o ler a string inteira.
	
	if(r==NULL)
		printf("Erro na leitura");
	else
		printf("%s", text);
	
		
	// 3 -  Fecha o arquivo
	fclose(f);
	
	
	return 0;
}
