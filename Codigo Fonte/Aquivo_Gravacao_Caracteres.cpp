// ESCRITA DE CAR�CTERES EM UM ARQUIVO


#include<stdio.h>
#include<stdlib.h>
#include<string.h> // Biblioteca para trabalhar com texto

int main(){
	
	// 1 - Ponteiro para um arquivo 
	FILE *f;
	
	// 2 - Abrindo um arquivo
	f = fopen("C:\\Arquivos\\texto.txt", "w"); 
	
	if(f==NULL){
		printf("Erro ao criar o arquivo");
		exit(1);
	}
	
	// Escrever dentro do arquivo que foi aberto no modo escrita
	char text[] = "Os arquivos em C podem ser binarios ou de texto";
	int i;
	
	for(i=0; i<strlen(text); i++)
		fputc(text[i], f); // Essa fun��o escreve letra por letra.
	
	
	// 3 -  Fecha o arquivo
	fclose(f);
	
	
	return 0;
}
