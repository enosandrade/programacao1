// FUN��O: PASSAGEM POR VALOR x PASSAGEM POR REFER�NCIA
#include <stdio.h>

int soma(int a, int b){
	a=20; b=18;
	return a + b;
}


int soma(int *x, int *y){
	*x=20;	*y=30;
	
	return *x + *y;
}

int main(){
	
	int a = 5, b = 8;
	printf("\n %d + %d = %d", a, b, soma(a, b));
	
	
	int x = 10, y = 5;
	printf("\n %d + %d = %d", x, y, soma(&x, &y));
	
	return 0;
}


