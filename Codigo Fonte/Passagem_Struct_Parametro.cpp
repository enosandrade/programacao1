// PASSAGEM DE STRUCT POR PAR�METRO
#include <stdio.h>

struct ponto{
	int x, y, z;
};

void atribui_1(struct ponto *p){
	(*p).x=5;
	(*p).y=10;
	(*p).z=15;
}

void atribui_2(struct ponto *p){
	p->x=3;
	p->y=1;
	p->z=8;
}

void atribui_3(struct ponto p){
	p.x=5;
	p.y=6;
	p.z=10;
	
	printf("\(%d, %d, %d\) \n", p.x, p.y, p.z);
}

int main(){
	struct ponto p1;
	
	atribui_1(&p1);
	
	printf("\(%d, %d, %d\) \n", p1.x, p1.y, p1.z);
	
	atribui_2(&p1);
	
	printf("\(%d, %d, %d\) \n", p1.x, p1.y, p1.z);
	
	atribui_3(p1);
	
	printf("\(%d, %d, %d\) \n", p1.x, p1.y, p1.z);
	
	return 0;
}


