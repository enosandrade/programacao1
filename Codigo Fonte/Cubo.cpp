#include <stdio.h>

int main(){
	float m[3][5][4] = {
					{ {0.0, 0.1, 0.2, 0.3}, 
				      {1.0, 1.1, 1.2, 1.3}, 
				      {2.0, 2.1, 2.2, 2.3}, 
				      {3.0, 3.1, 3.2, 3.3},
				      {4.0, 4.1, 4.2, 4.3}},
				      
				    { {0.0, 0.1, 0.2, 0.3}, 
				      {1.0, 1.1, 1.2, 1.3}, 
				      {2.0, 2.1, 2.2, 2.3}, 
				      {3.0, 3.1, 3.2, 3.3},
				      {4.0, 4.1, 4.2, 4.3}},
				      
				    { {0.0, 0.1, 0.2, 0.3}, 
				      {1.0, 1.1, 1.2, 1.3}, 
				      {2.0, 2.1, 2.2, 2.3}, 
				      {3.0, 3.1, 3.2, 3.3},
				      {4.0, 4.1, 4.2, 4.3}}
										};
	int P, L, C;
	
	//Percorre o cubo de dados 
	for(P=0; P<3; P++){// Anda matriz por matriz
		printf("\n");		
		for(L=0; L<5; L++){ // Anda linha por linha da matriz
			printf("\n");
			for(C=0; C<4; C++){ // Anda coluna por coluna da matriz				
					printf("%.1f ", m[P][L][C]);
				}
			}
	}
	
	
	return 0;	
}

